FROM maven:3.8.4-openjdk-17-slim

WORKDIR /usr/app
COPY src /usr/app/src
COPY pom.xml /usr/app

RUN mvn clean install -DskipTests
