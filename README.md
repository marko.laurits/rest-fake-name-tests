# Test automation suite for [rest-fake-name](https://gitlab.com/n4468/rest-fake-name) project

## Setup
### System configuration
- Java 17+ (follow [this](https://docs.oracle.com/en/java/javase/17/install/) instructions)
- Maven 3.8 (reach out to [this link](https://docs.oracle.com/en/java/javase/17/install/))
- It might be required to restart your machine and/or configure environment variables based on OS. More info [here](https://www.java.com/en/download/help/path.html)

### Using Docker
Run docker container based on the image that has Java and Maven with correct versions:
`docker run -it --rm -v $PWD:/app maven:3.8.4-openjdk-17-slim bash`

It will start a new terminal inside docker container where you can work with this project without any local pre-setup.
Here you can execute any java or maven commands for this project inside app folder (switch to it using `cp app`).

## Build and test
Project building and tests execution are the same for local and docker console
```shell
mvn install -DskipTests
mvn test
```
As the result of tests run you should see that 2 tests are passed.

### Run tests on localhost
If you want to test service running on localhost, execute it with additional parameter:
```shell
mvn test -DENV=local
```
### Run tests from docker container
If you run both microservice and tests in different docker container *but in the same docker network*, 
you can call endpoint via docker0 IP that you can obtain with command `docker network inspect bridge --format='{{(index .IPAM.Config 0).Gateway}}'`

Usually it's `172.17.0.1` that is configured in this test project, then you can run tests with:
```shell
mvn test -DENV=docker
```
In case if you have IP different from default one, please execute:
```shell
mvn test -DENV=docker -DSERVICE_HOST=xxx.xx.x.x
```